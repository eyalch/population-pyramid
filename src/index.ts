import { buildPopulationPyramid } from './pyramid'
import { Gender, Person, PopulationPyramid } from './types'

function countFemaleAbove20InPyramid(pyramid: PopulationPyramid): number {
  return pyramid.reduce(
    (count, group) => count + (group.bottom >= 20 ? group.female : 0),
    0,
  )
}

const persons: Person[] = [
  { age: 20, gender: Gender.Male },
  { age: 55, gender: Gender.Female },
  { age: 12, gender: Gender.Female },
  { age: 34, gender: Gender.Male },
  { age: 23, gender: Gender.Male },
  { age: 5, gender: Gender.Male },
  { age: 19, gender: Gender.Female },
  { age: 42, gender: Gender.Female },
  { age: 66, gender: Gender.Male },
  { age: 103, gender: Gender.Male },
  { age: 50, gender: Gender.Female },
  { age: 71, gender: Gender.Female },
  { age: 4, gender: Gender.Male },
  { age: 145, gender: Gender.Female },
  { age: 1, gender: Gender.Female },
  { age: 80, gender: Gender.Male },
  { age: 49, gender: Gender.Female },
  { age: 14, gender: Gender.Male },
  { age: 8, gender: Gender.Female },
  { age: -2, gender: Gender.Female },
  { age: 44, gender: Gender.Male },
  { age: 0, gender: Gender.Male },
]

const myPyramid = buildPopulationPyramid(persons, 5)

console.log(
  'Female above the age of 20 in the pyramid:',
  countFemaleAbove20InPyramid(myPyramid),
)
