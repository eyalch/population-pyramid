export enum Gender {
  Male,
  Female,
  Anonymous,
}

export interface Person {
  age: number
  gender?: Gender
}

export interface AgeGroup {
  bottom: number
  top: number
  male: number
  female: number
  anonymous: number
}

export type PopulationPyramid = AgeGroup[]
