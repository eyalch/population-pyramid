import { Gender, Person, PopulationPyramid } from './types'

export function buildPopulationPyramid(
  persons: Person[] | number[],
  ageGroupSize = 5,
): PopulationPyramid {
  if (ageGroupSize < 1) return []

  persons = cleanPersons(persons)
  if (!persons.length) return []

  // initialize the pyramid with empty age groups
  const pyramid: PopulationPyramid = []
  const maxAge = Math.max.apply(Math, persons.map(p => p.age)) || 1
  for (let i = 0; i <= maxAge; i += ageGroupSize) {
    pyramid.push({
      bottom: i,
      top: i + ageGroupSize - 1,
      male: 0,
      female: 0,
      anonymous: 0,
    })
  }

  persons.forEach(person => {
    const ageGroup = pyramid[Math.floor(person.age / ageGroupSize)]
    if (person.gender === Gender.Male) {
      ageGroup.male++
    } else if (person.gender === Gender.Female) {
      ageGroup.female++
    } else {
      ageGroup.anonymous++
    }
  })

  return pyramid
}

// cleanPersons converts ages to `Person`s and
// filters-out persons with age NOT between 0 and 130
function cleanPersons(persons: Array<Person | number>): Person[] {
  return persons
    .map<Person>(person => {
      if (typeof person === 'number') {
        return { age: person, gender: Gender.Anonymous }
      } else if (person.gender == null) {
        return { ...person, gender: Gender.Anonymous }
      }
      return person
    })
    .filter(person => person.age >= 0 && person.age <= 130)
}
