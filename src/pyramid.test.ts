import { buildPopulationPyramid } from './pyramid'
import { Gender, PopulationPyramid } from './types'

function countPersonsInPyramid(pyramid: PopulationPyramid): number {
  return pyramid.reduce(
    (count, group) => count + group.male + group.female + group.anonymous,
    0,
  )
}

function countPersonsInPyramidByGender(pyramid: PopulationPyramid): number[] {
  let maleCount = 0
  let femaleCount = 0
  for (const ageGroup of pyramid) {
    maleCount += ageGroup.male
    femaleCount += ageGroup.female
  }
  return [maleCount, femaleCount]
}

test('pyramid should have the same amount of persons as passed (valid) persons', () => {
  const pyramid = buildPopulationPyramid([
    { age: 1 },
    { age: 4 },
    { age: 10 },
    { age: 13 },
  ])
  expect(countPersonsInPyramid(pyramid)).toBe(4)
})

test('pyramid should ignore invalid ages', () => {
  const pyramid = buildPopulationPyramid([
    { age: 0 },
    { age: -1 },
    { age: 0 },
    { age: 200 },
  ])
  expect(countPersonsInPyramid(pyramid)).toBe(2)
})

test('pyramid should accept an array of ages', () => {
  const pyramid = buildPopulationPyramid([1, 4, 10, 13])
  expect(countPersonsInPyramid(pyramid)).toBe(4)
})

test('pyramid should be filled with empty age groups till the highest age', () => {
  let pyramid = buildPopulationPyramid([99], 10)
  expect(pyramid.length).toBe(10)

  pyramid = buildPopulationPyramid([79], 20)
  expect(pyramid.length).toBe(4)
})

test('pyramid should have the same amount of male and female as passed to it', () => {
  const pyramid = buildPopulationPyramid(
    [
      { age: 10, gender: Gender.Male },
      { age: 20, gender: Gender.Female },
      { age: 30, gender: Gender.Male },
      { age: 40, gender: Gender.Female },
      { age: 50, gender: Gender.Male },
    ],
    10,
  )
  const [maleCount, femaleCount] = countPersonsInPyramidByGender(pyramid)
  expect(maleCount).toBe(3)
  expect(femaleCount).toBe(2)
})
